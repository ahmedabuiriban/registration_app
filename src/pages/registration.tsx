import React, {useState} from "react";
import RegistrationForm from "../components/RegistrationForm/registrationForm";
import {connect} from "react-redux";
import {postPaymentData, saveUserInformation} from "../store/actions";

export interface RegistrationProps {
    userData: User,
    _currentStep: number,
    paymentDataId: string,
    error: string,
    saveUserInformation(user: User, currentStep: number): void;
    postPaymentData(user: User): void;
}

const Registration: React.FC<RegistrationProps> = ({
                                                       saveUserInformation,
                                                       postPaymentData,
                                                       userData,
                                                       _currentStep,
                                                       paymentDataId,
                                                       error
                                                   }) => {

    const [currentStep, setCurrentStep] = useState<number>(_currentStep || 1)


    return (
        <>
            <h5 className="register-header text-white bg-dark">Registration App</h5>
            <RegistrationForm currentStep={currentStep}
                              setCurrentStep={setCurrentStep}
                              paymentDataId={paymentDataId}
                              saveUserInformation={saveUserInformation}
                              postPaymentData={postPaymentData}
                              error={error}
                              userData={userData}/>
        </>
    );
}

const mapStateToProps = (state: any) => {
    return {
        userData: state.user,
        _currentStep: state.currentStep,
        paymentDataId: state.paymentDataId,
        error: state.error
    };
}


const mapDispatchToProps = (dispatch: Function) => {
    return {
        saveUserInformation: (user: User, currentStep: number) => dispatch(saveUserInformation(user, currentStep)),
        postPaymentData: (user: User) => dispatch(postPaymentData(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration);