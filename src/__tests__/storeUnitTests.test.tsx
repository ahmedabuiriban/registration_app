const configureMockStore = require('redux-mock-store').default;
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import nextId from "react-id-generator";

import * as actions from '../store/actions';
import store from '../store';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);
const mock = new MockAdapter(axios);
const _store = mockStore();

const testUser: User = {
    id: nextId(),
    firstName: 'Ahmed',
    lastName: 'Abuiriban',
    telephone: 1000,
    address: {
        street: 'Abu baker',
        homeNumber: 102,
        zipCode: 97000,
        city: 'Gaza'
    },
    accountOwner: 'ahmedeoo',
    iban: '01548006'
}

test('Save user information', () => {
  store.dispatch(actions.saveUserInformation(testUser , 1));
  let user = store.getState().user;
  let currentStep = store.getState().currentStep;
  expect(user.firstName).toBe('Ahmed');
  expect(user.lastName).toBe('Abuiriban');
  expect(user.telephone).toBe(1000);
  expect(user.accountOwner).toBe('ahmedeoo');
  expect(user.iban).toBe('01548006');
  expect(user.address.street).toBe('Abu baker');
  expect(user.address.homeNumber).toBe(102);
  expect(user.address.zipCode).toBe(97000);
  expect(user.address.city).toBe('Gaza');
  expect(currentStep).toBe(1);
});

describe('post payment details', () => {
    beforeEach(() => {
        _store.clearActions();
    });
    test('dispatches SUCCESS_POST_PAYMENT_DATA after a successful API requests', () => {
        mock.onPost('/savePaymentData').reply(200, { paymentDataId: '#78ychu647126764' })
        _store.dispatch(actions.postPaymentData(testUser)).then(()=>{
            let expectedActions = [
                { type: 'SUCCESS_POST_PAYMENT_DATA', paymentDataId: '#78ychu647126764' }
            ]
            expect(_store.getActions()).toEqual(expectedActions)
        })
    });

    test('dispatches FAILED_POST_PAYMENT_DATA after a FAILED API requests', () => {
        mock.onPost('/savePaymentData').reply(500,{});
        _store.dispatch(actions.postPaymentData(testUser)).then(() => {
            let expectedActions = [
                {
                    type: 'FAILED_POST_PAYMENT_DATA',
                    message: "Error: Request failed with status code 500",
                }
            ]
            expect(_store.getActions()).toEqual(expectedActions)
        });
    });
});