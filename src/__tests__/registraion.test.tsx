import React from "react";
import {render} from "@testing-library/react";
import {Provider} from "react-redux";
import nextId from "react-id-generator";

import RegistrationForm, {RegistrationFormProps} from "../components/RegistrationForm/registrationForm";
import store from '../store';


const testUser: User = {
    id: nextId(),
    firstName: '',
    lastName: '',
    telephone: null,
    address: {
        street: '',
        homeNumber: null,
        zipCode: null,
        city: ''
    },
    accountOwner: '',
    iban: ''
}

function renderRegistrationForm(props: Partial<RegistrationFormProps> = {}) {
    const defaultProps: RegistrationFormProps = {
        saveUserInformation() {
            return;
        },
        postPaymentData() {
            return;
        },
        setCurrentStep() {
        },
        userData: testUser,
        currentStep: 1,
        paymentDataId: '',
        error: ''
    };
    return render(<Provider store={store}>
        <RegistrationForm {...defaultProps} {...props} />
    </Provider>);
}

describe("<Registration />", () => {
    test("should display a blank form in step 1", async () => {
        const {findByTestId} = renderRegistrationForm();
        const registrationForm = await findByTestId("registration-form");
        expect(registrationForm).toHaveFormValues({
            firstName: "",
            lastName: "",
            telephone: null,
        });
    });

    test("should display a blank form in step 2", async () => {
        const {findByTestId} = renderRegistrationForm({currentStep: 2});
        const registrationForm = await findByTestId("registration-form");
        expect(registrationForm).toHaveFormValues({
            street: '',
            homeNumber: null,
            zipCode: null,
            city: '',
        });
    });

    test("should display a blank form in step 3", async () => {
        const {findByTestId} = renderRegistrationForm({currentStep: 3});
        const registrationForm = await findByTestId("registration-form");
        expect(registrationForm).toHaveFormValues({
            accountOwner: '',
            iban: ''
        });
    });
});
