interface Address {
    street: string,
    homeNumber: number,
    zipCode: number,
    city: string
}

interface User {
    id: string;
    firstName: string;
    lastName: string;
    telephone: number;
    address: Address;
    accountOwner: string;
    iban: string
}

type UserAction = {
    type: string
    user: User
    currentStep: number
}

type UserState = {
    user: User,
    currentStep: number,
    paymentDataId: string,
    error: string
}

type SuccessAction = {
    type: string,
    paymentDataId: string
}

type FailedAction = {
    type: string,
    message: string
}