import axios from "axios";

export const SAVE_USER_INFORMATION = "SAVE_USER_INFORMATION"

export function saveUserInformation(user: User, currentStep: number) {
    const action: UserAction = {
        type: SAVE_USER_INFORMATION,
        user,
        currentStep
    }
    localStorage.setItem('userInformation', JSON.stringify(user));
    localStorage.setItem('currentStep', currentStep.toString());
    return (dispatch: Function) => {
        dispatch(action)
    }
}

export const SUCCESS_POST_PAYMENT_DATA = "SUCCESS_POST_PAYMENT_DATA"
export const FAILED_POST_PAYMENT_DATA = "FAILED_POST_PAYMENT_DATA"

export const postPaymentData = (user: User) => (dispatch: Function) => {
    return axios.post('/savePaymentData', {
        customerId: user.id,
        iban: user.iban,
        owner: user.accountOwner
    }).then(response => {
        if (response.status === 200) {
            const successAction: SuccessAction = {
                type: SUCCESS_POST_PAYMENT_DATA,
                paymentDataId: response.data['paymentDataId']
            }
            dispatch(successAction)
        }
    }).catch(error => {
        const failedAction: FailedAction = {
            type: FAILED_POST_PAYMENT_DATA,
            message: error.toString()
        }
        dispatch(failedAction)
    });
}
