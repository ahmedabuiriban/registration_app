import nextId from "react-id-generator";

import * as actionTypes from "../actions";

const user = JSON.parse(localStorage.getItem('userInformation'));
const currentStep = parseInt(localStorage.getItem('currentStep'));

const clearedUser: User = {
        id: nextId(),
        firstName: '',
        lastName: '',
        telephone: undefined,
        address: {
            street: '',
            homeNumber: undefined,
            zipCode: undefined,
            city: ''
        },
        accountOwner: '',
        iban: ''
    }

const initialState: UserState = {
    user: user || clearedUser,
    currentStep: currentStep || 1,
    paymentDataId: '',
    error: ''
}


const userReducer = (
    state: UserState = initialState,
    action: any
): UserState => {
    switch (action.type) {
        case actionTypes.SAVE_USER_INFORMATION:
            const newUser: User = action.user
            const newCurrentStep: number = action.currentStep
            return {
                ...state,
                user: newUser,
                currentStep: newCurrentStep
            }
        case actionTypes.SUCCESS_POST_PAYMENT_DATA:
            const paymentDataId: string = action.paymentDataId
            localStorage.removeItem('userInformation')
            localStorage.removeItem('currentStep')
            return {
                ...state,
                user: clearedUser,
                paymentDataId,
                currentStep: 1
            }
        case actionTypes.FAILED_POST_PAYMENT_DATA:
            const error: string = action.message
            return {
                ...state,
                error
            }
    }
    return state
}

export default userReducer