import React, {ReactElement} from 'react';

import "./style.scss";

interface ModalFormProps {
    visibility: boolean,
    onCancel: any,
    children: React.ReactNode,
    title: string
}

const Modal: React.FC<ModalFormProps> = ({children, visibility, onCancel, title}) => {
    const showHideClassName = visibility ? "modal display-block" : "modal display-none";

    return (
        <div className={showHideClassName}>
            <section className="modal-main">
                <div className="modal-header bg-dark text-white">
                    <h5 className="modal-title">{title}</h5>
                    <button type="button" className="close text-white"
                            onClick={onCancel}
                            aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                {children}
            </section>
        </div>
    )
};

export default Modal;