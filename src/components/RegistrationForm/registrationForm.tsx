import React, {useState, ChangeEvent} from "react";
import nextId from "react-id-generator";

import "./style.scss";
import Modal from "../Modal";
import {PersonalInformation} from "./steps/personalInformation";
import {AddressInformation} from "./steps/addressInformation";
import {PaymentInformation} from "./steps/paymentInformation";


export interface RegistrationFormProps {
    userData: User,
    currentStep: number;
    paymentDataId: string,
    error: string,

    setCurrentStep(stepNumber: number): void;

    saveUserInformation(user: User, currentStep: number): void;

    postPaymentData(user: User): void;
}


const RegistrationForm: React.FC<RegistrationFormProps> = ({
                                                               currentStep,
                                                               setCurrentStep,
                                                               saveUserInformation,
                                                               postPaymentData,
                                                               userData,
                                                               paymentDataId,
                                                               error
                                                           }) => {
    const [userInformation, setUserInformation] = useState<User>(userData || {
        id: nextId(),
        firstName: '',
        lastName: '',
        telephone: null,
        address: {
            street: '',
            homeNumber: null,
            zipCode: null,
            city: ''
        },
        accountOwner: '',
        iban: ''
    })
    const [showSuccessModal, setShowSuccessModal] = useState<boolean>(false)
    const [showFailedModal, setShowFailedModal] = useState<boolean>(false)

    React.useEffect(() => {
        if (error) {
            setShowFailedModal(true)
        }
        if (paymentDataId) {
            setShowSuccessModal(true)
            setUserInformation({
                id: nextId(),
                firstName: '',
                lastName: '',
                telephone: null,
                address: {
                    street: '',
                    homeNumber: null,
                    zipCode: null,
                    city: ''
                },
                accountOwner: '',
                iban: ''
            })
            setCurrentStep(1);
        }
    }, [error, paymentDataId])

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target
        // change address properties
        if (currentStep == 2) {
            setUserInformation({
                ...userInformation,
                address: {
                    ...userInformation.address,
                    [name]: value
                }
            })
        } else {
            setUserInformation({
                ...userInformation,
                [name]: value
            })
        }
    };

    const onNextOrSubmit = async (e: React.FormEvent) => {
        e.preventDefault();
        if (currentStep === 3) {
            postPaymentData(userInformation)
        }
        // If the current step is 1 or 2, then add one on "next" button click
        currentStep = currentStep >= 2 ? 3 : currentStep + 1
        setCurrentStep(currentStep);
        saveUserInformation(userInformation, currentStep);
    }

    const back = () => {
        // If the current step is 2 or 3, then subtract one on "previous" button click
        currentStep = currentStep <= 1 ? 1 : currentStep - 1
        setCurrentStep(currentStep);
    }

    return (
        <>
            <form data-testid="registration-form" onSubmit={onNextOrSubmit}>
                {currentStep == 1 && <PersonalInformation
                    handleChange={handleChange}
                    userInformation={userInformation}
                />}
                {currentStep == 2 && <AddressInformation
                    handleChange={handleChange}
                    userInformation={userInformation}
                />}
                {currentStep == 3 && <PaymentInformation
                    handleChange={handleChange}
                    userInformation={userInformation}
                />}
                <div className="btn-group">
                    {currentStep != 1 && <input
                        type="button" name="previous" className="previous action-button"
                        value="Previous" onClick={back}/>}
                    <input type="submit" name={currentStep === 3 ? 'submit' : 'next'} className="next action-button"
                           value={currentStep === 3 ? 'Submit' : 'Next'}/>
                </div>
            </form>
            <Modal visibility={showSuccessModal} onCancel={() => setShowSuccessModal(false)} title="Success">
                <div className="modal-content">Payment Id: {paymentDataId}</div>
            </Modal>
            <Modal visibility={showFailedModal} onCancel={() => setShowFailedModal(false)} title="Failed">
                <div className="modal-content">{error}</div>
            </Modal>
        </>

    );
}


export default RegistrationForm;