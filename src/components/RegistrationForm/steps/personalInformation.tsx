import React, {ChangeEvent} from "react";

interface PersonalInformationProps {
    handleChange(e: ChangeEvent<HTMLInputElement>): void;

    userInformation: User
}

export const PersonalInformation: React.FC<PersonalInformationProps> = ({handleChange, userInformation}) => {
    return (
        <>
            <h3 className="step-title">Enter your personal information</h3>
            <div className="form-group">
                <label htmlFor="first-name" className="form-label">First Name *</label>
                <input type="text" name="firstName" placeholder="First Name" className="text-input" required
                       onChange={(e) => handleChange(e)} id="first-name"
                       value={userInformation.firstName}/>
            </div>
            <div className="form-group">
                <label htmlFor="recipient-name" className="form-label">Last Name *</label>
                <input type="text" name="lastName" placeholder="Last Name" className="text-input" required
                   onChange={(e) => handleChange(e)} id="last-name"
                   value={userInformation.lastName}/>
            </div>
            <div className="form-group">
                <label htmlFor="telephone" className="form-label">Telephone *</label>
                <input type="number" name="telephone" placeholder="Telephone" className="text-input" required
                   onChange={(e) => handleChange(e)} id="telephone"
                   value={userInformation.telephone}/>
            </div>
        </>
    );
}