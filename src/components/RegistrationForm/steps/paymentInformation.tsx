import React, {ChangeEvent} from "react";

interface PaymentInformationProps {
    handleChange(e: ChangeEvent<HTMLInputElement>): void;

    userInformation: User
}

export const PaymentInformation: React.FC<PaymentInformationProps> = ({handleChange, userInformation}) => {
    return (
        <>
            <h2 className="step-title">Enter your payment information</h2>
            <div className="form-group">
                <label htmlFor="accountOwner" className="form-label">Account owner *</label>
                <input type="text" name="accountOwner" placeholder="Account owner" className="text-input" required
                       onChange={(e) => handleChange(e)} id="accountOwner"
                       value={userInformation.accountOwner}/>
            </div>
            <div className="form-group">
                <label htmlFor="iban" className="form-label">IBAN *</label>
                <input type="text" name="iban" placeholder="IBAN" className="text-input" required
                   onChange={(e) => handleChange(e)} id="iban"
                   value={userInformation.iban}/>
            </div>
        </>
    );
}