import React, {ChangeEvent} from "react";

interface AddressInformationProps {
    handleChange(e: ChangeEvent<HTMLInputElement>): void;

    userInformation: User
}

export const AddressInformation: React.FC<AddressInformationProps> = ({handleChange, userInformation}) => {
    return (
        <>
            <h2 className="step-title">Enter your address information</h2>
            <div className="form-group">
                <label htmlFor="street" className="form-label">Street *</label>
                <input type="text" name="street" placeholder="Street" className="text-input" required
                       onChange={(e) => handleChange(e)} id="street"
                       value={userInformation.address.street}/>
            </div>
            <div className="form-group">
                <label htmlFor="homeNumber" className="form-label">Home Number *</label>
                <input type="number" name="homeNumber" placeholder="Home number" className="text-input" required
                   onChange={(e) => handleChange(e)} id="homeNumber"
                   value={userInformation.address.homeNumber}/>
            </div>
            <div className="form-group">
                <label htmlFor="zipCode" className="form-label">Zip Code *</label>
                <input type="number" name="zipCode" placeholder="zipCode" className="text-input" required
                   onChange={(e) => handleChange(e)} id="zipCode"
                   value={userInformation.address.zipCode}/>
            </div>
            <div className="form-group">
                <label htmlFor="city" className="form-label">City *</label>
                <input type="text" name="city" placeholder="city" className="text-input" required
                   onChange={(e) => handleChange(e)} id="city"
                   value={userInformation.address.city}/>
            </div>

        </>
    );
}