import React from 'react';
import {Provider} from 'react-redux';
import store from './store';
import Registration from './pages/registration';
import "./global.scss";
import * as ReactDOM from "react-dom";

export class App extends React.Component {
    public render(): JSX.Element {
        return (
            <Provider store={store}>
                <div className="app">
                    <Registration/>
                </div>
            </Provider>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById("app"));
