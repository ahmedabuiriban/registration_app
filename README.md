## Registration app


### Possible optimizations for your code:-

1. Test form components.
2. Test store state after dispatch actions.
3. Create error handling reducer.

### Things could be done better:-

1. Create stages bar so user can know more about registration process and its stages.
2. Use another font libraries.
